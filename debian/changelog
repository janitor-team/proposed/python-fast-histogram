fast-histogram (0.9-2) unstable; urgency=medium

  * Disable modification of version.py file during package build time.
    (Closes: #978270)
  * Mark autopkgtest as superficial (Closes: #971464)
  * Bump package Standards-Version to 4.5.1. No changes required
  * debian/watch: Bump file standard version to 4

 -- Josue Ortega <josue@debian.org>  Sat, 02 Jan 2021 14:41:28 -0600

fast-histogram (0.9-1) unstable; urgency=medium

  * New upstream release (0.9-1)
  * Remove debian/patch/00-fix-test-array-generation.patch. Patch no
    longer needed
  * Bump debhelper compat to use (= 13)
  * Bump Standards-Version to 4.5.0. No changes required
  * debian?control: Add python3-setuptools-scm as build dependency
  * Fix autopackage test in order to avoid false positives due to relative
    package import

 -- Josue Ortega <josue@debian.org>  Sun, 12 Jul 2020 21:15:14 -0600

fast-histogram (0.7-2) unstable; urgency=medium

  * Add patch to fix test_histogram (Closes: #945413)
  * Set package to use debhelper-compat 12
  * Bump Stadards-Version to 4.4.1. No changes required

 -- Josue Ortega <josue@debian.org>  Tue, 17 Dec 2019 09:49:05 -0600

fast-histogram (0.7-1) unstable; urgency=medium

  * New upstream release (0.7)
  * Bumps Standards-Version to 4.3.0:
    - debian/control: Documents Rules-Requires-Root field.
  * Add CI tests

 -- Josue Ortega <josue@debian.org>  Tue, 22 Jan 2019 07:27:35 -0600

fast-histogram (0.5-1) unstable; urgency=medium

  * New upstream release 0.5.
  * debian/control:
    - Change python3-dev for python3-all-dev to support
    all python3 installed versions. (Closes: #910319)
    - Add python3-pytest as Build Dependency.
  * Bumps Standards-Version to 4.2.1, no changes required.

 -- Josue Ortega <josue@debian.org>  Mon, 08 Oct 2018 11:59:51 -0600

fast-histogram (0.4-1) unstable; urgency=medium

  * Initial release (Closes: #898069).

 -- Josue Ortega <josue@debian.org>  Sun, 06 May 2018 11:57:45 -0600
